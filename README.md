# FreeBSD dotfiles

## Notes after upgraded to FreeBSD 14

Reinstall `pkg` and upgrade:

```sh
pkg-static install -f pkg
pkg update
pkg upgrade
```

For the *Intel videocard*:

```
pkg install drm-kmod
```

Had to change `TERM` in `.bashrc` from `xterm-256color` to:

```sh
export TERM=xterm-clear
```

to ensure *terminal clear* after exit from `mc`, `vim`, `nnn`, etc.

## Notes to self

- Run this: `mkdir ~/bin`
- `~/bin` is in `PATH` by default in the default shell but not in `bash`!
- For the *Trash can* functionality in a GUI file manager install `gvfs`!
- For guest additions add this packages: `virtualbox-ose-additions` and
  `virtualbox-ose-kmod`
- Create `.fonts` for local fonts
- Create `.config/fontconfig/fonts.conf` for local fonts options
- Add `set t_Co=256` in `~/.vimrc`.
- To compile from `suckless.org` install `pkgconf`!
- When compiling `dwm` change `config.mk` like this:

    ```C
    X11INC = /usr/local/include
    X11LIB = /usr/local/lib
    
    FREETYPEINC = /usr/local/include/freetype2
    ```

