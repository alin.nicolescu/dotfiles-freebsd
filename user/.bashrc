if [[ $- != *i* ]]; then
    return
fi

shopt -s autocd
shopt -s checkwinsize
shopt -s no_empty_cmd_completion
shopt -s histappend

export HISTCONTROL=ignoredups

export TERMCMD=/usr/local/bin/st
export EDITOR=vim
export PAGER=less

export TERM=xterm-clear
# export TERM=xterm-256color

# Path
TEXLIVE_PATH="$HOME/opt/texlive/2021/bin/amd64-freebsd"
export PATH="$TEXLIVE_PATH:$HOME/bin:$HOME/.local/bin:$PATH"

#export LANG=ro_RO.UTF-8
export LANG=en_US.UTF-8
export MM_CHARSET=UTF-8

export GPG_TTY=$(tty)

export NNN_OPENER=~/bin/nnnopen.sh
export NNN_OPTS="dReo"

export QT_QPA_PLATFORMTHEME=qt5ct

if [ -f /usr/local/share/bash-completion/bash_completion ]
then
    . /usr/local/share/bash-completion/bash_completion
fi

if [ -f ~/.bash_aliases ]
then
    . ~/.bash_aliases
fi

# Colors for less
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# Prompt
RESET="\e[0m"
GREEN="\e[32m"
YELLOW="\e[93m"
RED="\e[91m"
BLUE="\e[34m"

export PS1="$GREEN\H$RESET [$BLUE\W$RESET] $RED\$$RESET "

