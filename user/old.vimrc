" Plugins
call plug#begin('~/.vim/plugged')
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
call plug#end()

" vim-airline settings
let g:airline_powerline_fonts=0
let g:airline_symbols_ascii=1
let g:airline_theme='gruvbox'
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#formatter='unique_tail'

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_symbols.crypt=''

" nerdtree settings
let NERDTreeShowHidden=1
"" Close vim when NERDTree is the only window left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Basic settings
set nocompatible
set t_Co=256
set encoding=utf-8
set cm=blowfish2
let mapleader = "\<Space>"
set spelllang=ro
set bs=2
set path+=**
set scrolloff=5

filetype plugin on
filetype indent on
syntax on

" Fix weird characters that popup sometimes
set t_RV=

" Colorscheme
"colorscheme dracula
set background=dark
colorscheme gruvbox

set colorcolumn=81
"set cursorline
set laststatus=2
set noshowmode

set number
set nowrap
set showmatch

" Splits
set splitbelow
set splitright

" Searching
set incsearch
set nohlsearch
set smartcase
set ignorecase

" Tab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set expandtab
set smarttab

" Visualize tabs, spaces, line endings etc.
set listchars=tab:»·,eol:↲,nbsp:␣,trail:·,extends:⟩,precedes:⟨

" Enable bash-like command line completion
set wildmenu

" Backup and swap
set nobackup
set noswapfile

" Disable automatic commenting on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Mappings
nnoremap <Leader>l :set list!<CR>
nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <Leader>r :set relativenumber!<CR>
"nnoremap <Leader>s :set spell!<CR>
nnoremap <Leader>s :setlocal spell! spelllang=en_us,ro<CR>

"" Tabs
nnoremap tn :tabnew<CR>
nnoremap tj :tabnext<CR>
nnoremap tk :tabprevious<CR>

"" Buffers
nnoremap bj :bnext<CR>
nnoremap bk :bprevious<CR>

"" Insert an actual TAB with Shift+TAB
inoremap <S-Tab> <C-V><Tab>

"" Remap split navigation to just CTRL + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

"" Remap Esc to jj
imap jj <Esc>

"" Adjust split sizes easier
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize -3<CR>
noremap <silent> <C-Down> :resize +3<CR>

"" Compile and run C++ files
autocmd filetype cpp nnoremap <Leader>. <Esc>:w<CR>:!clear && clang++ % -o %:r && ./%:r<CR>
autocmd filetype tex nnoremap <Leader>. <Esc>:w<CR>:!clear && pdflatex %<CR>
