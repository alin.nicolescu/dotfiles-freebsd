#!/bin/sh

AC=$(sysctl -n hw.acpi.acline)
BATT_PERCENT=$(sysctl -n hw.acpi.battery.life)
BATT_STATE=""
case $(sysctl -n hw.acpi.battery.state) in
    "1")
        BATT_STATE="discharging"
        ;;
    "2")
        BATT_STATE="charging"
        ;;
esac
PRINT="BATT $BATT_PERCENT% $BATT_STATE"
if [ $AC = "0" ]; then
    echo $PRINT
else
    echo "AC | $PRINT"
fi

