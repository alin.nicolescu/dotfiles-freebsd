#!/bin/sh
cpu=$(vmstat | awk 'NR==3 {print $(NF-2)+$(NF-1) "\%"}')
if [ $? -eq 0 ]; then
    printf "CPU %s" "$cpu"
else
    printf "CPU ?"
fi
