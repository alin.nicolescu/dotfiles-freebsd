#!/bin/sh

ETH=$(ifconfig em0 | grep inet)
if [ "$ETH" != "" ]
then
    ETH="ETH up"
else
    ETH="ETH down"
fi

WIFI=$(ifconfig wlan0 | grep inet)
if [ "$WIFI" != "" ]
then
    WIFI="WIFI up"
else
    WIFI="WIFI down"
fi

echo "$ETH | $WIFI"
