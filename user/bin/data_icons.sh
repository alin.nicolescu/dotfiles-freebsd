#!/usr/bin/env bash
d=$(date +"%d %b")
t=$(date +"%H:%M")
printf " %s  %s" "$d" "$t"
