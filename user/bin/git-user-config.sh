#!/usr/bin/env bash

git config user.name "Alin Nicolescu"
git config user.email "nicolescu.cnmv@gmail.com"
git config user.signingkey 70197EF57ABCFF2E
git config commit.gpgsign true
