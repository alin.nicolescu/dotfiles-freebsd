#!/usr/bin/env sh

if [ ! -f "$1" ]; then
	exit
fi

MIME1=$(file -b --mime-type "$1")
MIME2=$(echo "$MIME1" | cut -d/ -f1)
MIME3=$(echo "$MIME1" | cut -d/ -f2)

if [ "$MIME1" = "application/epub+zip" ] || \
	[ "$MIME1" = "application/pdf" ] || \
	[ "$MIME1" = "application/postscript" ] || \
	[ "$MIME1" = "image/vnd.djvu" ]; then
	zathura "$1" > /dev/null 2>&1 &
elif [ "$MIME2" = "text" ]; then
	nnnedit.sh "$1"
elif [ "$MIME2" = "image" ]; then
	sxiv "$1" > /dev/null 2>&1 &
elif [ "$MIME2" = "video" ]; then
	mpv "$1" > /dev/null 2>&1 &
elif [ "$MIME2" = "audio" ]; then
	if [ "$MIME3" = "mpeg" ]; then
		killall mpg123 > /dev/null 2>&1
		$TERMCMD -e  "mpg123 -v $1" &
	else
		killall mpv > /dev/null 2>&1
		mpv --force-window=yes "$1" > /dev/null 2>&1 &
	fi
else
	xdg-open "$1"
fi

