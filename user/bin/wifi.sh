#!/bin/sh

IP=$(ifconfig wlan0 | grep inet | cut -w -f3)
if [ "$IP" != "" ]
then
    SSID=$(ifconfig wlan0 | grep ssid | cut -w -f3)
    WIFI="WiFi $SSID"
else
    WIFI="WiFi DOWN"
fi

echo "$WIFI"
