#!/bin/sh

IP1=$(ifconfig em0 | grep inet | cut -w -f3)
if [ "$IP1" != "" ]
then
    ETH=" $IP1"
else
    ETH=" down"
fi

IP2=$(ifconfig wlan0 | grep inet | cut -w -f3)
if [ "$IP2" != "" ]
then
    WIFI=" $IP2"
else
    WIFI=" down"
fi

echo "$ETH | $WIFI"
