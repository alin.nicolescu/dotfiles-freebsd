" Plugins
call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'preservim/nerdtree'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
"Plug 'ryanoasis/vim-devicons'
call plug#end()

" Status line
let g:currentmode={
       \ 'n'  : 'NORMAL ',
       \ 'v'  : 'VISUAL ',
       \ 'V'  : 'V·Line ',
       \ "\<C-V>" : 'V·Block ',
       \ 'i'  : 'INSERT ',
       \ 'R'  : 'R ',
       \ 'Rv' : 'V·Replace ',
       \ 'c'  : 'Command ',
       \}

set statusline=
set statusline+=%#IncSearch#
set statusline+=\ %{toupper(g:currentmode[mode()])}
set statusline+=%#CursorLineNr#
set statusline+=%m
set statusline+=%#StatusLine#
set statusline+=%=
set statusline+=%#NonText#
set statusline+=\ %f
set statusline+=\ 
set statusline+=%#CursorLineNr#
set statusline+=%y
set statusline+=%r
set statusline+=%#IncSearch#
set statusline+=\ %p%%
set statusline+=\ %l/%L
set statusline+=\ [%c]

" Nerdtree settings
let NERDTreeShowHidden=1
"" Close vim when NERDTree is the only window left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Basic settings
set nocompatible
set t_Co=256
set encoding=utf-8
set cm=blowfish2
let mapleader = "\<Space>"
set spelllang=ro
set bs=2
set path+=**
set scrolloff=5
filetype plugin on
filetype indent on
syntax on

" Colorscheme
set background=dark
colorscheme gruvbox

set colorcolumn=81
"set cursorline
set laststatus=2
set noshowmode

set number
set nowrap
set showmatch

" Splits
set splitbelow
set splitright

" Searching
set incsearch
set nohlsearch
set smartcase
set ignorecase

" Tab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set expandtab
set smarttab

" Visualize tabs, spaces, line endings etc.
set listchars=tab:»·,eol:↲,nbsp:␣,trail:·,extends:⟩,precedes:⟨

" Enable bash-like command line completion
set wildmenu

" Backup and swap
set nobackup
set noswapfile

" Disable automatic commenting on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Mappings
nnoremap <Leader>l :set list!<CR>
nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <Leader>r :set relativenumber!<CR>
"nnoremap <Leader>s :set spell!<CR>
nnoremap <Leader>s :setlocal spell! spelllang=en_us,ro<CR>

"" Tabs
nnoremap tn :tabnew<CR>
nnoremap tj :tabnext<CR>
nnoremap tk :tabprevious<CR>

"" Buffers
nnoremap bj :bnext<CR>
nnoremap bk :bprevious<CR>

"" Insert an actual TAB with Shift+TAB
inoremap <S-Tab> <C-V><Tab>

"" Remap split navigation to just CTRL + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

"" Remap Esc to jj
imap jj <Esc>

"" Adjust split sizes easier
noremap <silent> <C-Left> :vertical resize +2<CR>
noremap <silent> <C-Right> :vertical resize -2<CR>
noremap <silent> <C-Up> :resize -2<CR>
noremap <silent> <C-Down> :resize +2<CR>

"" Compile and run C++ files
autocmd filetype c nnoremap <Leader>. <Esc>:w<CR>:!clear && cc % -o %:r && ./%:r<CR>
autocmd filetype cpp nnoremap <Leader>. <Esc>:w<CR>:!clear && clang++ % -o %:r && ./%:r<CR>
autocmd filetype tex nnoremap <Leader>. <Esc>:w<CR>:!clear && pdflatex %<CR>

"" Moving lines
nnoremap <silent> <S-Down> :m .+1<CR>==
nnoremap <silent> <S-Up> :m .-2<CR>==
inoremap <silent> <S-Down> <Esc>:m .+1<CR>==gi
inoremap <silent> <S-Up> <Esc>:m .-2<CR>==gi
vnoremap <silent> <S-Down> :m '>+1<CR>gv=gv
vnoremap <silent> <S-Up> :m '<-2<CR>gv=gv
