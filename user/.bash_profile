if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

[ -z "$DISPLAY" ] && [ "$(tty)" = "/dev/ttyv0" ] && exec startx
