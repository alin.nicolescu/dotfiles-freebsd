#alias ls='ls --color=always'
#alias ll='ls -l'
#alias la='ls -la'
alias ls='exa -F --sort=type'
alias la='exa -a -F --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias tmux='tmux -u -2'
alias xterm='uxterm'
alias weather='curl wttr.in?0Q'
alias xopen='xdg-open'
alias git-log="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias git-simple-log="git log --pretty=oneline --abbrev-commit"
